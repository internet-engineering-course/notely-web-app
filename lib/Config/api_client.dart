import 'dart:convert';

import 'package:dio/dio.dart';

import 'package:get_storage/get_storage.dart';
import 'package:note/Widget/snackbar.dart';

import 'app_urls.dart';

enum HttpMethod { get, post, put, delete }

class ApiClient {
  final Dio dio = Dio(BaseOptions(
    baseUrl: url,
    headers: {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    },
  ));
  Future<Response> dioRequest({
    required String urlPath,
    required HttpMethod httpMethod,
    Map<String, dynamic>? body,
    bool needToken = true,
  }) async {
    String token = GetStorage().read('token') ?? '';
    Response response;
    try {
      response = httpMethod == HttpMethod.put
          ? await dio.put(
              urlPath,
              data: json.encode(body),
              options: Options(
                  headers: {if (needToken) 'Authorization': 'Bearer $token'}),
            )
          : httpMethod == HttpMethod.post
              ? await dio.post(
                  urlPath,
                  data: json.encode(body),
                  options: Options(headers: {
                    if (needToken) 'Authorization': 'Bearer $token'
                  }),
                )
              : httpMethod == HttpMethod.get
                  ? await dio.get(
                      urlPath,
                      options: Options(headers: {
                        if (needToken) 'Authorization': 'Bearer $token'
                      }),
                    )
                  : await dio.delete(
                      urlPath,
                      data: json.encode(body),
                      options: Options(headers: {
                        if (needToken) 'Authorization': 'Bearer $token'
                      }),
                    );
      return response;
    } on DioException catch (e) {
      if (e.response == null) {
        snackBarWidget(
            messageText: e.message ?? "server error!",
            type: SnackBarWidgetType.failure);
      } else if (e.type == DioExceptionType.badResponse) {
        snackBarWidget(
            title: 'Error: ${e.response!.statusCode}',
            messageText: "bad response error!",
            type: SnackBarWidgetType.failure);
      } else if (e.type == DioExceptionType.connectionTimeout) {
        snackBarWidget(
            title: 'Error: ${e.response!.statusCode}',
            messageText: "connection timeout error!",
            type: SnackBarWidgetType.failure);
      } else if (e.type == DioExceptionType.receiveTimeout) {
        snackBarWidget(
            title: 'Error: ${e.response!.statusCode}',
            messageText: "response timeout error!",
            type: SnackBarWidgetType.failure);
      } else {
        snackBarWidget(
            title: 'Error: ${e.response!.statusCode}',
            messageText:
                'Error ${e.response!.statusCode}: ${e.response!.statusMessage}',
            type: SnackBarWidgetType.failure);
      }
      return Response(
          requestOptions: RequestOptions(),
          statusCode: 2,
          statusMessage: "Error");
    }
  }
}
