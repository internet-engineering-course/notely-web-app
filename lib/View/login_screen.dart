import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

import '../Controller/auth_controller.dart';
import '../Widget/text_field.dart';

class LoginScreen extends StatelessWidget {
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();
  final RxBool isLoading = false.obs;
  LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
          body: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 16,
                      ),
                      Text(
                        "NOTELY",
                        style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                            fontWeight: FontWeight.bold, fontFamily: "Titan"),
                      ),
                      const SizedBox(
                        height: 60,
                      ),
                      Text(
                        "Login",
                        style: Theme.of(context).textTheme.bodyLarge,
                      ),
                      const SizedBox(
                        height: 52,
                      ),
                      Form(
                        key: formkey,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 24,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Padding(
                                padding: EdgeInsets.fromLTRB(8, 16, 0, 4),
                                child: Text("Email Address"),
                              ),
                              TextFieldWidget(
                                validator: (value) {
                                  if (value != null && value.isEmpty) {
                                    return '* Please enter your Email!';
                                  } else if (!RegExp(
                                          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                      .hasMatch(value!)) {
                                    return '* The email is incorrect!';
                                  } else {
                                    return null;
                                  }
                                },
                                hintText: "Exapmle@gmail.com",
                                prefixIconConstraints:
                                    const BoxConstraints(maxWidth: 32),
                                controller: AuthController.to.emailTec,
                              ),
                              const Padding(
                                padding: EdgeInsets.fromLTRB(8, 16, 0, 4),
                                child: Text("Password"),
                              ),
                              TextFieldWidget(
                                validator: (value) {
                                  if (value != null && value.isEmpty) {
                                    return '* Please enter your Password!';
                                  } else if (value!.length < 8) {
                                    return '* Password must have at least 8 characters';
                                  } else {
                                    return null;
                                  }
                                },
                                obscureText: true,
                                hintText: "********",
                                prefixIconConstraints:
                                    const BoxConstraints(maxWidth: 32),
                                controller: AuthController.to.passwordTec,
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                    ],
                  ),
                ),
              ),
              Column(
                children: [
                  Padding(
                      padding: const EdgeInsets.fromLTRB(24, 0, 24, 8),
                      child: Obx(
                        () => ElevatedButton(
                          style: ButtonStyle(
                              shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(12)))),
                          onPressed: () async {
                            if (formkey.currentState!.validate() &&
                                !isLoading.value) {
                              isLoading.value = true;
                              await AuthController.to.login();
                              isLoading.value = false;
                            }
                          },
                          child: isLoading.value
                              ? const SpinKitThreeBounce(
                                  color: Colors.white,
                                  size: 24.0,
                                )
                              : Text(
                                  "Login",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyLarge!
                                      .copyWith(
                                          letterSpacing: 4,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                ),
                        ),
                      )),
                  TextButton(
                    onPressed: () {
                      Get.offNamed("/registerScreen");
                    },
                    child: Text(
                      "Don't have an account?",
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
