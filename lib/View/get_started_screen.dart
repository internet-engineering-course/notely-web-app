import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

class GetStartedScreen extends StatelessWidget {
  final RxBool isLoading = false.obs;
  GetStartedScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 16,
                    ),
                    Text(
                      "NOTELY",
                      style: Theme.of(context)
                          .textTheme
                          .bodyLarge!
                          .copyWith(fontWeight: FontWeight.bold, fontFamily: 'Titan'),
                    ),
                    const SizedBox(
                      height: 80,
                    ),
                    Image.asset(
                      "assets/images/getStartedIcon.png",
                      width: 250,
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: Text(
                        "World’s Safest And Largest Digital Notebook",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.w900,
                            fontSize: 22,
                            color: Color(0xff403B36)),
                      ),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: Text(
                        "Notely is the world’s safest, largest and intelligent digital notebook. Join over 10M+ users already using Notely.",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            
            Padding(
                padding: const EdgeInsets.fromLTRB(24, 0, 24, 8),
                child: Obx(
                  () => ElevatedButton(
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12)))),
                    onPressed: () {
                      isLoading.value = true;
                      Get.toNamed('/registerScreen');
                      isLoading.value = false;
                    },
                    child: isLoading.value
                        ? const SpinKitThreeBounce(
                            color: Colors.white,
                            size: 24.0,
                          )
                        : Text(
                            "GET STARTED",
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge!
                                .copyWith(
                                    letterSpacing: 4,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                          ),
                  ),
                )),
            TextButton(
                onPressed: () {
                  Get.toNamed("/loginScreen");
                },
                child: Text(
                  "Already have an account?",
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold),
                )),
            const SizedBox(
              height: 8,
            )
          ],
        ),
      ),
    );
  }
}
