import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:note/Controller/main_controller.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: FutureBuilder(
          future: MainController.to.getMainScreenData(),
          builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text("Main screen"),
                  const SizedBox(
                    height: 16,
                  ),
                  Text("wellcome ${MainController.to.email}"),
                  const SizedBox(
                    height: 32,
                  ),
                  SizedBox(
                    width: 150,
                    height: 50,
                    child: ElevatedButton(
                        onPressed: () {
                          GetStorage().remove('token');
                          Get.offAllNamed('/getStartedScreen');
                        },
                        child: const Text("Logout")),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
