import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:note/Controller/auth_controller.dart';
import 'package:note/Widget/text_field.dart';

class RegisterScreen extends StatelessWidget {
  final GlobalKey<FormState> formkey = GlobalKey<FormState>();
  final RxBool isLoading = false.obs;

  RegisterScreen({super.key});
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
          resizeToAvoidBottomInset: true,
          body: Column(children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 16,
                    ),
                    Text(
                      "NOTELY",
                      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          fontWeight: FontWeight.bold, fontFamily: "Titan"),
                    ),
                    const SizedBox(
                      height: 60,
                    ),
                    Text(
                      "Create a free account",
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 32),
                      child: Text(
                          "Join Notely for free. Create and share unlimited notes with your friends.",
                          textAlign: TextAlign.center),
                    ),
                    const SizedBox(
                      height: 52,
                    ),
                    Form(
                      key: formkey,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 24,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Padding(
                              padding: EdgeInsets.fromLTRB(8, 0, 0, 4),
                              child: Text("Full Name"),
                            ),
                            TextFieldWidget(
                              validator: (value) {
                                if (value != null && value.isEmpty) {
                                  return '* Please enter your Full Name!';
                                } else {
                                  return null;
                                }
                              },
                              hintText: "Shirin Mohebi",
                              prefixIconConstraints:
                                  const BoxConstraints(maxWidth: 32),
                              controller: AuthController.to.fullNameTec,
                            ),
                            const Padding(
                              padding: EdgeInsets.fromLTRB(8, 16, 0, 4),
                              child: Text("Email Address"),
                            ),
                            TextFieldWidget(
                              validator: (value) {
                                if (value != null && value.isEmpty) {
                                  return '* Please enter your Email!';
                                } else if (!RegExp(
                                        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                    .hasMatch(value!)) {
                                  return '* The email is incorrect!';
                                } else {
                                  return null;
                                }
                              },
                              hintText: "Exapmle@gmail.com",
                              prefixIconConstraints:
                                  const BoxConstraints(maxWidth: 32),
                              controller: AuthController.to.emailTec,
                            ),
                            const Padding(
                              padding: EdgeInsets.fromLTRB(8, 16, 0, 4),
                              child: Text("Password"),
                            ),
                            TextFieldWidget(
                              obscureText: true,
                              validator: (value) {
                                if (value != null && value.isEmpty) {
                                  return '* Please enter your Password!';
                                } else if (value!.length < 8) {
                                  return '* Password must have at least 8 characters';
                                } else {
                                  return null;
                                }
                              },
                              hintText: "********",
                              prefixIconConstraints:
                                  const BoxConstraints(maxWidth: 32),
                              controller: AuthController.to.passwordTec,
                            ),
                            const Padding(
                              padding: EdgeInsets.fromLTRB(8, 16, 0, 4),
                              child: Text("Confirm Password"),
                            ),
                            TextFieldWidget(
                              validator: (value) {
                                if (value != null && value.isEmpty) {
                                  return '* Please enter Confirm Password!';
                                } else if (value !=
                                    AuthController.to.passwordTec.text) {
                                  return "* The password does not match";
                                } else {
                                  return null;
                                }
                              },
                              obscureText: true,
                              hintText: "********",
                              prefixIconConstraints:
                                  const BoxConstraints(maxWidth: 32),
                              controller:
                                  AuthController.to.confirmPasswordTec,
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 16,)
                  ],
                ),
              ),
            ),
            Padding(
                padding: const EdgeInsets.fromLTRB(24, 0, 24, 8),
                child: Obx(
                  () => ElevatedButton(
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12)))),
                    onPressed: () async {
                      if (formkey.currentState!.validate() &&
                          !isLoading.value) {
                        isLoading.value = true;
                        await AuthController.to.register();
                        isLoading.value = false;
                      }
                    },
                    child: isLoading.value
                        ? const SpinKitThreeBounce(
                            color: Colors.white,
                            size: 24.0,
                          )
                        : Text(
                            "Create Account",
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge!
                                .copyWith(
                                    letterSpacing: 4,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                          ),
                  ),
                )),
            TextButton(
                onPressed: () {
                  Get.offNamed("/loginScreen");
                },
                child: Text(
                  "Already have an account?",
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold),
                )),
            const SizedBox(
              height: 8,
            )
          ]),
        ),
      ),
    );
  }
}
