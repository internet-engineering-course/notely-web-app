import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFieldWidget extends StatefulWidget {
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  final String? labelText;
  final bool? autoFocus;
  final TextEditingController? controller;
  final bool? enabled;
  final FocusNode? focusNode;
  final List<TextInputFormatter>? inputFormatters;
  final TextInputType? keyboardType;
  final int? maxLength;
  final void Function(String)? onChanged;
  final TextInputAction? textInputAction;
  final void Function()? onPressed;
  final bool? obscureText;
  final bool? readOnly;
  final void Function()? onTap;
  final String? Function(String?)? validator;
  final TextAlign? textAlign;
  final String? initialValue;
  final String? hintText;
  final TextDirection? textDirection;
  final BoxConstraints? prefixIconConstraints;
  final BoxConstraints? suffixIconConstraints;
  final int? maxLines;
  const TextFieldWidget({
    Key? key,
    this.prefixIcon,
    this.suffixIcon,
    this.labelText,
    this.autoFocus = false,
    this.controller,
    this.enabled = true,
    this.focusNode,
    this.inputFormatters,
    this.keyboardType,
    this.maxLength,
    this.onChanged,
    this.textInputAction = TextInputAction.done,
    this.onPressed,
    this.obscureText = false,
    this.validator,
    this.textAlign,
    this.initialValue,
    this.textDirection,
    this.prefixIconConstraints,
    this.suffixIconConstraints,
    this.onTap,
    this.readOnly = false,
    this.maxLines = 1,
    this.hintText,
  }) : super(key: key);
  @override
  State<TextFieldWidget> createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  bool isShowPassword = true;
  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: const MediaQueryData(textScaleFactor: 1.0),
      child: TextFormField(
        scrollPadding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        decoration: InputDecoration(
          hintText: widget.hintText,
          hintStyle: TextStyle(color: Colors.grey.withOpacity(0.5)),
          filled: true,
          fillColor: const Color(0xFFFFFDFA),
          border: OutlineInputBorder(
            borderSide: const BorderSide(color: Color(0xFFF2E5D5), width: 1),
            borderRadius: BorderRadius.circular(12),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(
                color: Color(
                  0xFFF2E5D5,
                ),
                width: 1),
            borderRadius: BorderRadius.circular(12),
          ),
          prefixIcon: Icon(
            widget.prefixIcon,
            color: Colors.black54,
            size: 20,
          ),
          suffixIcon: widget.obscureText!
              ? GestureDetector(
                  onTap: () {
                    setState(() {
                      isShowPassword = !isShowPassword;
                    });
                  },
                  child: Icon(
                    isShowPassword ? Icons.visibility : Icons.visibility_off,
                    color: Colors.black54,
                    size: 20,
                  ),
                )
              : IconButton(
                  onPressed: widget.onPressed,
                  icon: Icon(
                    widget.suffixIcon,
                    color: Colors.black54,
                    size: 20,
                  ),
                ),
          contentPadding: const EdgeInsets.all(24),
          errorStyle: const TextStyle(fontSize: 10, color: Colors.red),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(
                color: Color.fromARGB(255, 85, 80, 74), width: 1),
            borderRadius: BorderRadius.circular(12),
          ),
          labelStyle: const TextStyle(fontSize: 12, color: Colors.black54),
          labelText: widget.labelText,
          counterText: '',
          prefixIconConstraints: widget.prefixIconConstraints,
          suffixIconConstraints: widget.suffixIconConstraints,
        ),
        autofocus: widget.autoFocus!,
        controller: widget.controller,
        cursorColor: Colors.black,
        cursorWidth: 0.5,
        enabled: widget.enabled,
        focusNode: widget.focusNode,
        inputFormatters: widget.inputFormatters,
        keyboardType: widget.keyboardType,
        maxLength: widget.maxLength,
        obscureText: widget.obscureText! ? isShowPassword : widget.obscureText!,
        onChanged: widget.onChanged,
        style: widget.enabled!
            ? const TextStyle(fontSize: 14, color: Colors.black)
            : const TextStyle(fontSize: 14, color: Colors.black54),
        textAlign: widget.textAlign ?? TextAlign.left,
        textDirection: widget.textDirection,
        textInputAction: widget.textInputAction,
        validator: widget.validator,
        initialValue: widget.initialValue,
        readOnly: widget.readOnly!,
        onTap: widget.onTap,
        maxLines: widget.maxLines,
      ),
    );
  }
}
