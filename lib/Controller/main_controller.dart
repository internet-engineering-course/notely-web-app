import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

class MainController extends GetxController {
  static MainController get to => Get.put<MainController>(MainController());
  String token = '';
  String email = '';
  Future<void> getMainScreenData() async {
    token = await GetStorage().read('token');
    final Map<String, dynamic> decodedToken = JwtDecoder.decode(token);
    email = decodedToken['sub'];
  }
}
