import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:note/Config/api_client.dart';
import 'package:note/Widget/snackbar.dart';

class AuthController extends GetxController {
  static AuthController get to => Get.put<AuthController>(AuthController());

  final ApiClient apiClient = ApiClient();
  final TextEditingController fullNameTec =
      TextEditingController();
  final TextEditingController emailTec =
      TextEditingController();
  final TextEditingController passwordTec =
      TextEditingController();
  final TextEditingController confirmPasswordTec =
      TextEditingController();

  Future<void> register() async {
    final response = await apiClient.dioRequest(
      urlPath: '/register',
      httpMethod: HttpMethod.post,
      body: {
        "fullName": fullNameTec.text,
        "emailAddress": emailTec.text,
        "password": passwordTec.text,
      },
      needToken: false,
    );
    if (response.statusCode == 200) {
      snackBarWidget(
          messageText: response.data['message'],
          type: SnackBarWidgetType.success);
      Get.offNamed("/loginScreen");
    }
  }

  Future<void> login() async {
    final response = await apiClient.dioRequest(
        urlPath: '/login',
        httpMethod: HttpMethod.post,
        body: {"emailAddress": emailTec.text, "password": passwordTec.text},
        needToken: false);

    if (response.statusCode == 200) {
      await GetStorage().write("token", response.data['access_token']);
      Get.offAllNamed('/mainScreen');
    }
  }
}
